farm evaluation
====
scripts and R functions to perform evaluation of FARM

###### Author
[Giovanni Bonafè](mailto:giovanni.bonafe@arpa.fvg.it)

###### Output
* AQ network observations vs model:
    + hourly time series
    + daily boxplots
    + 24h boxplots
    + skill scores
* model QA/QC for ISO 9001:
    + latency
    + discrepancy
