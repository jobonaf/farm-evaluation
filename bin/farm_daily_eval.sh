#!/bin/bash -   
#title          :farm_daily_eval.sh
#description    :evaluation of daily runs of FARM
#author         :Giovanni Bonafe'
#date           :20200908
#version        :1.3.0
#usage          :./farm_daily_eval.sh
#notes          :       
#bash_version   :4.1.2(1)-release
#============================================================================

.  ~/.bash_profile

# paths
root_dir="/u/arpa/bonafeg/src/farm_eval"
R_dir=${root_dir}"/R"
out_dir=${root_dir}"/out"
log_dir=${root_dir}"/log"
bin_dir=${root_dir}"/bin"
pbs_file=${root_dir}/farm_daily_eval.pbs

# transition from PBS to PBSPRO
day=$(date +"%Y%m%d")
if [ $day -gt 20180215 ] ; then
	numnodes="select"
	numcores="ncpus"
else
	numnodes="nodes"
	numcores="ppn"
fi

# launch PBS, launching R script
cat << EOF > ${pbs_file}
#PBS -l walltime=04:00:00
#PBS -l ${numnodes}=1:${numcores}=32
#PBS -V
#PBS -q hp

module purge
module load netcdf-fortran/4.5.2/gcc r-rgdal nco/4.7.9

export root_dir=${root_dir}
cd ${root_dir}
Rscript ${R_dir}/run_farm_daily_eval.R
mv ${root_dir}/*pdf ${out_dir}

${bin_dir}/farm_daily_dashboard.sh PM10
${bin_dir}/farm_daily_dashboard.sh O3
${bin_dir}/farm_daily_dashboard.sh NO2
EOF
qsub -N farm_daily_eval -e ${log_dir} -o ${log_dir} ${pbs_file}

