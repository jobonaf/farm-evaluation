#!/bin/bash -   
#title          :farm_daily_dashboard.sh
#description    :FARM daily dashboard
#author         :G.Bonafe'
#date           :20190410
#version        :1.0    
#usage          :./farm_daily_dashboard.sh PM10/O3/NO2
#notes          :       
#bash_version   :4.1.2(1)-release
#============================================================================

# paths
root_dir="/u/arpa/bonafeg/src/farm_eval"
tpl_dir=${root_dir}"/templates"
pub_dir="/u/arpa/bonafeg/public_html/FARM_daily_eval/"

# arguments, usage
if [ $# -lt 1 ] ; then echo "usage: $0 PM10/NO2/O3"; exit ; fi
if [ $1 == "--help" -o $1 == "-h" ] ; then echo "usage: $0 PM10/NO2/O3"; exit ; fi
poll=$1

# prepare Rmd
fileRmd=${poll}.Rmd
cat ${tpl_dir}/farm_daily_eval.Rmd.tpl | sed s/%%POLLUTANT%%/${poll}/g > $fileRmd

# compile Rmd
module load proj gdal
/u/arpa/bonafeg/utilities/convert/Rmd2html.sh $fileRmd
mv ${poll}.html ${pub_dir}
